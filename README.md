# Oreon Runs

<h1>Modern application for orienteering.</h1>

This is a graduation work project focused on creating a fully functional application for orienteering. The entire app runs on Python, or to be more specific, on the Kivy framework. It is meant to be multi-platform and it mostly works that way, although Apple plays hard to get, so it is a bit harder to get it running on it.

<p>&nbsp;</p>


**Core features**

The app is split into few sepparate functions. One let's you create your own track and store it into a QR code string and other let's you load up the previously created QR code string and load the map. This allows you or anyone else to make custom tracks and share them among others. Of course it was meant mainly for school purposes, but for now it stays in the developement and later it may even go out in the public. For now the app has functional menu, QR encoder, scanner and decoder, QR code string loading mechanism, GPS tracking system (not very accurate one), interactive map, run-tracking interface and more. 

<p>&nbsp;</p>


**A powerful framework**

The Kivy framework works well in basically any application but as far as I am concerned, there is not such a vast multi-platform application that uses anything similar to this project. So in that case, it works as a nice base for mostly any app you would need. There are many features that has been configured to work alongside others, so there is nothing easier than just getting rid of what you don't need and leaving just the important stuff to use for your own work.

<p>&nbsp;</p>


**Current state of developement**

As I mentioned, the app is still in developement, despite the graduation project being already submitted and graded. The work currently stopped at a point, where further developement is either way too complex or just out of my priority right now. The project isn't dead, but it will take some time to be finished. Nevertheless, the app is in somewhat working state, there are few things that need to be smoothened, but core features are done.