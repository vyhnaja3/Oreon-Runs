# Privacy Policy

## Access to the device

Oreon Runs uses Wifi, GPS and storage access to work properly. Wifi is used to properly load map data, GPS is used to properly track your movement on the run and storage is used only for storing exported QR codes of tracks and checkpoints with user's interaction. All access is necessary for the proper functionality of the aplication and is only sharing necesarry data locally. GPS location and storage are not shared outside the device and are **only stored locally**.


## Data Collection

Oreon Runs collects only necessary data for it's functionality, such as GPS location. Data collected by the aplication are not shared and are stored only locally on the device. 


## Supported Versions

Any version bellow 1.1.0 is officially **not** supported as a secure and LTS version due to developement reasons. Versions above 1.1.0 are the official versions that are securely supported.

| Version | Supported          |
| ------- | ------------------ |
|> 1.1.0 | :white_check_mark: |
|  1.0.0 | :x:                |
| < 0.9.9.x | :x:                |

## Reporting a Vulnerability

You can report any problems or vulnerability through an issue, pull request or to author's email directly if it is not suited for the wider audience. 
Reports will be taken care of in fastest time as possible and repaired in due time. 
Any reports of vulnerability are accepted.
